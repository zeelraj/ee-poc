import React, { FC, Fragment } from "react";
import { InputProps } from "../../interfaces/index";
import { StyledInput, StyledInputLabel, StyledInputMessage, StyledInputText } from '../../styled/index';

const Input: FC<InputProps> = ({
    id,
    disabled,
    label,
    message,
    error,
    success,
    onChange,
    placeholder,
    ...props
}) => {
    return (
        <Fragment>
            <StyledInputLabel>
                <StyledInputText disabled={disabled} error={error}>
                    {label}
                </StyledInputText>
            </StyledInputLabel>
            <StyledInput
                id={id}
                type="text"
                onChange={onChange}
                disabled={disabled}
                error={error}
                success={success}
                placeholder={placeholder}
                {...props} />
            <StyledInputMessage>
                <StyledInputText error={error}>{message}</StyledInputText>
            </StyledInputMessage>
        </Fragment>
    );
};

export default Input;