import { ChangeEventHandler, MouseEventHandler } from "react";

type ReactText = string | number;

export interface ButtonProps {
    children: ReactText,
    disabled?: boolean;
    onClick?: MouseEventHandler<HTMLButtonElement>;
    // CSS
    size?: "small" | "medium" | "large";
    color?: string;
    backgroundColor?: string;
    borderRadius?: number;
}

export interface InputProps {
    id?: string;
    label?: string;
    error?: boolean;
    message?: string;
    success?: boolean;
    disabled?: boolean;
    placeholder?: string;
    onChange?: ChangeEventHandler<HTMLInputElement>;
    // CSS
    size?: string | number;
    color?: string;
    margin?: string | number;
    padding?: string | number;
    backgroundColor?: string;
    borderRadius?: number;
}