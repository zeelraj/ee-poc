import React, { FC } from "react";
import { StyledButton } from "../../styled";
import { ButtonProps } from "../../interfaces";

const Button: FC<ButtonProps> = ({
    children,
    size,
    disabled,
    onClick,
    ...props
}) => {
    return (
        <StyledButton type="button"
            onClick={onClick}
            disabled={disabled}
            size={size}
            {...props}>
            {children}
        </StyledButton>
    );
};

export default Button;