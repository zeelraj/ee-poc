import styled from "styled-components";
import { InputProps } from "../interfaces";

export const StyledInput = styled.input<InputProps>`
  height: 40px;
  width: 300px;
  border-radius: ${p => p.borderRadius ? `${p.borderRadius}px` : '3px'};
  border: solid 2px
    ${(props) =>
        props.disabled
            ? "#e4e3ea"
            : props.error
                ? "#a9150b"
                : props.success
                    ? "#067d68"
                    : "#353637"};
  color: ${p => p.color || '#000000'};
  background-color: ${p => p.backgroundColor || '#FFFFFF'};
  &:focus {
    border: solid 2px #1b116e;
  }
`;

export const StyledInputLabel = styled.div<InputProps>`
  font-size: 14px;
  color: ${(props) => (props.disabled ? "#e4e3ea" : "#080808")};
  padding-bottom: 6px;
`;

export const StyledInputMessage = styled.div<InputProps>`
  font-size: 14px;
  color: "#a9150b8";
  padding-top: 4px;
`;

export const StyledInputText = styled.p<InputProps>`
  margin: 0px;
  color: ${(props) =>
        props.disabled ? "#e4e3ea" : props.error ? "#a9150b" : "#080808"};
`;