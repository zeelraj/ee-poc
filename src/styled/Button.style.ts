import styled from "styled-components";
import { ButtonProps } from "../interfaces";

export const StyledButton = styled.button<ButtonProps>`
    border: 0;
    line-height: 1;
    font-size: 15px;
    cursor: pointer;
    font-weight: 700;
    font-weight: bold;
    border-radius: ${(props) => props.borderRadius ? `${props.borderRadius}px` : '3px'};
    display: inline-block;
    padding: ${(props) =>
        props.size === "small"
            ? "7px 25px 8px"
            : props.size === "medium"
                ? "9px 30px 11px"
                : "14px 30px 16px"};
    color: ${(props) => (props.color || "#ffffff")};
    background-color: ${(props) => (props.backgroundColor || "#1b116e")};
    opacity: ${(props) => (props.disabled ? 0.5 : 1)};
    &:active {
    border: solid 2px #1b116e;
    padding: ${(props) =>
        props.size === "small"
            ? "5px 23px 6px"
            : props.size === "medium"
                ? "7px 28px 9px"
                : "12px 28px 14px"};
    }
`;