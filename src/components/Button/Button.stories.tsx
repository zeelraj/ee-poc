import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import Button from "./Button";

const meta: Meta<typeof Button> = {
    component: Button,
    title: "Button",
    argTypes: {},
};
export default meta;

type Story = StoryObj<typeof Button>;

export const Primary: Story = (args: any) => (
    <Button data-testId="InputField-id" {...args} />
);
Primary.args = {
    disabled: false,
    children: "Primary",
};

export const Disabled: Story = (args: any) => (
    <Button data-testId="InputField-id" {...args} />
);
Disabled.args = {
    disabled: true,
    children: "Disabled",
};

export const Small: Story = (args: any) => (
    <Button data-testId="InputField-id" {...args} />
);
Small.args = {
    disabled: false,
    size: "small",
    children: "Small",
};

export const Medium: Story = (args: any) => (
    <Button data-testId="InputField-id" {...args} />
);
Medium.args = {
    disabled: false,
    size: "medium",
    children: "Medium",
};

export const Large: Story = (args: any) => (
    <Button data-testId="InputField-id" {...args} />
);
Large.args = {
    disabled: false,
    size: "large",
    children: "Large",
};